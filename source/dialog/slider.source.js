function slider (a_init, a_tpl,lotteryid) {
    this.f_setValue  = f_sliderSetValue;
    this.lotteryid = lotteryid;
    this.f_addValue  = f_sliderAddValue;
    this.f_reduceValue  = f_sliderReduceValue;
    this.f_getPos    = f_sliderGetPos;
    // register in the global collection
    if (!window.A_SLIDERS){
        window.A_SLIDERS = [];
    }
    this.n_id = window.A_SLIDERS.length;
    window.isadjustprize = 1;//设置玩法是否可以调节奖金
    window.A_SLIDERS[this.n_id] = this;
    // save config parameters in the slider object
    var s_key;
    if (a_tpl){
        for (s_key in a_tpl){
            this[s_key] = a_tpl[s_key];
        }
    }
    for (s_key in a_init){
        this[s_key] = a_init[s_key];
    }
    this.n_pix2value = this.n_pathLength / (this.n_maxValue - this.n_minValue);
    if (this.n_value == null){
        this.n_value = this.n_minValue;
    }

    // generate the control's HTML float:left;
    $("#"+this.d_sliderdiv).html(
        '<div style="position:relative;margin-top:7px;width:' + this.n_controlWidth + 'px;height:' + this.n_controlHeight + 'px;border:0; background:url(' + this.s_imgControl + ') repeat-x 0 0" id="sl' + this.n_id + 'base">' +
        '<img src="' + this.s_imgSlider + '" width="' + this.n_sliderWidth + '" height="' + this.n_sliderHeight + '" border="0" style="position:absolute;left:' + this.n_pathLeft + 'px;top:' + this.n_pathTop + 'px;z-index:' + this.n_zIndex + ';cursor:pointer;visibility:hidden;" name="sl' + this.n_id + 'slider" id="sl' + this.n_id + 'slider" onmousedown="return f_sliderMouseDown(' + this.n_id + ')"/></div>'
        );
    this.e_base   = get_element('sl' + this.n_id + 'base');
    this.e_slider = get_element('sl' + this.n_id + 'slider');
	
    // safely hook document/window events
    if (!window.f_savedMouseMove && document.onmousemove != f_sliderMouseMove) {
        window.f_savedMouseMove = document.onmousemove;
        document.onmousemove = f_sliderMouseMove;
    }
    if (!window.f_savedMouseUp && document.onmouseup != f_sliderMouseUp) {
        window.f_savedMouseUp = document.onmouseup;
        document.onmouseup = f_sliderMouseUp;
    }
    // preset to the value in the input box if available
    var e_input = this.s_form == null ? get_element(this.s_name): document.forms[this.s_form]? document.forms[this.s_form].elements[this.s_name] : null;
    this.f_setValue(e_input && e_input.value != '' ? e_input.value : null, 0,this.lotteryid);
    this.e_slider.style.visibility = 'visible';
}

function f_sliderSetValue (n_value, b_noInputCheck,lotteryid) {
    if (n_value == null){
        n_value = this.n_value == null ? this.n_minValue : this.n_value;
    }
    if (isNaN(n_value)){
        return false;
    }
    // round to closest multiple if step is specified
    if (this.n_step){
        n_value = Math.round((n_value - this.n_minValue) / this.n_step) * this.n_step + this.n_minValue;
    }
    // smooth out the result
    if (n_value % 1){
        n_value = Math.round(n_value * 1e5) / 1e5;
    }

    if (n_value < this.n_minValue){
        n_value = this.n_minValue;
    }
    if (n_value > this.n_maxValue){
        n_value = this.n_maxValue;
    }
    this.n_value = n_value;
    // move the slider
    if (this.b_vertical){
        this.e_slider.style.top  = (this.n_pathTop + this.n_pathLength - Math.round((n_value - this.n_minValue) * this.n_pix2value)) + 'px';
    }else{
        this.e_slider.style.left = (this.n_pathLeft + Math.round((n_value - this.n_minValue) * this.n_pix2value)) + 'px';
    }
    // 新增
    $("#sl" + this.n_id + "base").css("background-position",(this.n_controlWidth+this.n_pathLeft + Math.round((n_value - this.n_minValue) * this.n_pix2value)) + 'px'+ ' 0');
    $("#"+this.s_name).val(n_value+"%");
    // 1. 显示控制
    if(this.showadjustprize == 1){
        $("#adprize-slider").css('display','');
    }else{
        $("#adprize-slider").css('display','none');
        $(".tbn_b_top").css("height","60px");
    }
    // 设置返点
    $("#pointbutton").html(n_value+"%");
    $("#pointbutton").attr("title",n_value+"%");
    // 设置奖金
    var current_prize = view_current_prize_short = view_current_prize_full = "";
    if(this.prize != null){// 
        var selmodes = $("#lt_project_modes").val();//元角模式
        var tmp_userpoint = this.userpoint;//用户返点
        var tmp_prizerate = this.prizerate;//奖金比率
        var tmp_adprizetype = this.adprizetype;//显示类型
        var j = 0;
        $.each(this.prize,function(i,n){
            //计算奖金 用户奖金 - （调整返点-用户返点）*奖金比率
            tmp_current_prize = Math.round(( n_value - tmp_userpoint )  * tmp_prizerate[i].totalmoney/tmp_prizerate[i].count * 100 ) / 10000;
            tmp_current_prize = Math.round((Number(n).toFixed(2) - tmp_current_prize.toFixed(2)) * 1000) / 1000;
            //console.info(tmp_current_prize);
            //显示调整
            if(j == 0){
                view_current_prize_short = tmp_current_prize.toString();
            }
            if(tmp_adprizetype == 1){//直接显示
                current_prize = view_current_prize_full = tmp_current_prize;
            }else if(tmp_adprizetype == 2){//多奖金求和后显示计算
                view_current_prize_short = current_prize = view_current_prize_full = Math.round((Number(view_current_prize_full) + tmp_current_prize) * 1000) / 1000;
            }else if(tmp_adprizetype == 3){//各个奖级分别显示计算
                if(j>0){
                    view_current_prize_full += "/";
                }else{
                    current_prize += Math.round(tmp_current_prize * 1000) / 1000;	
                }
                view_current_prize_full += tmp_current_prize.toString();
            }
            j++;
        });
    }
    $("#prizebutton").html(view_current_prize_short);
    $("#prizebutton").attr("title",view_current_prize_full);
    $("#"+this.s_tname).val(current_prize);
    if(b_noInputCheck != 0 && this.isadjustprize==1){
        SetCookie('dypoint'+this.lotteryid,n_value,86400);
    }
    window.isadjustprize = this.isadjustprize;
}

// add
function f_sliderAddValue () {
    if(window.isadjustprize==0){//不可调节奖金
        return ;
    }
    if (this.n_value != null){
        this.f_setValue(this.n_value + this.n_step, 1,this.lotteryid);
    }
}

// reduce
function f_sliderReduceValue () {
    if(window.isadjustprize==0){//不可调节奖金
        return ;
    }
    if (this.n_value != null){
        this.f_setValue(this.n_value - this.n_step, 1,this.lotteryid);
    }
}

// get absolute position of the element in the document
function f_sliderGetPos (b_vertical, b_base) {
    var n_pos = 0,s_coord = (b_vertical ? 'Top' : 'Left');
    var o_elem = o_elem2 = b_base ? this.e_base : this.e_slider;
	
    while (o_elem) {
        n_pos += o_elem["offset" + s_coord];
        o_elem = o_elem.offsetParent;
    }
    o_elem = o_elem2;

    var n_offset;
    while (o_elem.tagName != "BODY") {
        n_offset = o_elem["scroll" + s_coord];
        if (n_offset){
            n_pos -= o_elem["scroll" + s_coord];
        }
        o_elem = o_elem.parentNode;
    }
    return n_pos;
}

function f_sliderMouseDown (n_id) {
    if(window.isadjustprize==0){//不可调节奖金
        return false;
    }
    window.n_activeSliderId = n_id;
    return false;
}

function f_sliderMouseUp (e_event, b_watching) {
    if(window.isadjustprize==0){//不可调节奖金
        return false;
    }
    if (window.n_activeSliderId != null) {
        var o_slider = window.A_SLIDERS[window.n_activeSliderId];
        o_slider.f_setValue(o_slider.n_minValue + (o_slider.b_vertical ? (o_slider.n_pathLength - parseInt(o_slider.e_slider.style.top) + o_slider.n_pathTop) : (parseInt(o_slider.e_slider.style.left) - o_slider.n_pathLeft)) / o_slider.n_pix2value);
        if (b_watching){
            return;
        }
        window.n_activeSliderId = null;
    }
    if (window.f_savedMouseUp){
        return window.f_savedMouseUp(e_event);
    }
}

function f_sliderMouseMove (e_event) {
    if(window.isadjustprize==0){//不可调节奖金
        return false;
    }
    if (!e_event && window.event) {
        e_event = window.event;
    }
    // save mouse coordinates
    if (e_event) {
        window.n_mouseX = e_event.clientX + f_scrollLeft();
        window.n_mouseY = e_event.clientY + f_scrollTop();
    }

    // check if in drag mode
    if (window.n_activeSliderId != null) {
        var o_slider = window.A_SLIDERS[window.n_activeSliderId];

        var n_pxOffset;
        if (o_slider.b_vertical) {
            var n_sliderTop = window.n_mouseY - o_slider.n_sliderHeight / 2 - o_slider.f_getPos(1, 1) - 3;
            // limit the slider movement
            if (n_sliderTop < o_slider.n_pathTop){
                n_sliderTop = o_slider.n_pathTop;
            }
            var n_pxMax = o_slider.n_pathTop + o_slider.n_pathLength;
            if (n_sliderTop > n_pxMax){
                n_sliderTop = n_pxMax;
            }
            o_slider.e_slider.style.top = n_sliderTop + 'px';
            n_pxOffset = o_slider.n_pathLength - n_sliderTop + o_slider.n_pathTop;
        }else {
            var n_sliderLeft = window.n_mouseX - o_slider.n_sliderWidth / 2 - o_slider.f_getPos(0, 1) - 3;
            // limit the slider movement
            if (n_sliderLeft < o_slider.n_pathLeft){
                n_sliderLeft = o_slider.n_pathLeft;
            }
            var n_pxMax = o_slider.n_pathLeft + o_slider.n_pathLength;
            if (n_sliderLeft > n_pxMax){
                n_sliderLeft = n_pxMax;
            }
            o_slider.e_slider.style.left = n_sliderLeft + 'px';
            n_pxOffset = n_sliderLeft - o_slider.n_pathLeft;
        }
        if (o_slider.b_watch){
            f_sliderMouseUp(e_event, 1);
        }
        return false;
    }
	
    if (window.f_savedMouseMove){
        return window.f_savedMouseMove(e_event);
    }
}

// get the scroller positions of the page
function f_scrollLeft() {
    return f_filterResults ( window.pageXOffset ? window.pageXOffset : 0, document.documentElement ? document.documentElement.scrollLeft : 0, document.body ? document.body.scrollLeft : 0	);
}
function f_scrollTop() {
    return f_filterResults ( window.pageYOffset ? window.pageYOffset : 0, document.documentElement ? document.documentElement.scrollTop : 0, document.body ? document.body.scrollTop : 0 );
}
function f_filterResults(n_win, n_docel, n_body) {
    var n_result = n_win ? n_win : 0;
    if (n_docel && (!n_result || (n_result > n_docel))){
        n_result = n_docel;
    }
    return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

function f_sliderError (n_id, s_message) {
    alert("Slider #" + n_id + " Error:\n" + s_message);
    window.n_activeSliderId = null;
}

get_element = document.all ? function (s_id) {
    return document.all[s_id]
} : function (s_id) {
    return document.getElementById(s_id)
};