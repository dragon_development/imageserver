//

let numballArr = [2,5,8,7,3];
let download_dt = document.getElementsByClassName("download_dt")[0];
let cw = download_dt.offsetWidth;
let ch = download_dt.offsetHeight;
const D = 100; //小球直径


function Numball(x,y,r,text){
	this.x = x;
	this.y = y;
	this.r = r;
	this.text = text;
	
}
Numball.prototype = {
	createBall:function(r){
		this.ball = document.createElement('div');
		this.ball.className = 'ball';
		this.ball.innerHTML = this.text;
		this.ball.style.width = this.r + 'px';
		this.ball.style.height = this.r + 'px';
		this.ball.style.lineHeight = this.r + 'px';
		this.ball.style.top = this.y +'px' ;
		this.ball.style.left = this.x +'px' ;
		download_dt.appendChild(this.ball);
	},
	moveBall:function(){
		let self = this;
		let speed = this.randSpeed()

		let sx = speed.sx/4,
			sy = speed.sy/4,
		    yy = self.ball.offsetTop,
			xx = self.ball.offsetLeft;
		function move(){
			if(xx<D || xx>cw-D){
				sx = -sx;
			}
			if(yy<D || yy>ch-D){
				sy = -sy;
			}
			yy += sy;
			xx += sx;

			self.ball.style.left = xx + 'px';
			self.ball.style.top = yy + 'px';

			let obj = document.getElementsByClassName('ball')
			// self.boom(self,obj)

			requestAnimationFrame(move);
		}
		requestAnimationFrame(move);

	},
	randSpeed:function(){
		return {
			sx:Math.random()>0.5?-randNum(2,4):randNum(2,4),
			sy:Math.random()>0.5?randNum(3,5):-randNum(3,5),
			yy:this.ball.offsetTop,
			xx:this.ball.offsetLeft
		}
	},
	boom:function(source,tarObj){//碰撞检测

		for(let i=0;i<tarObj.length;i++){
			// console.log(tarObj[i])
		}
	}
}

function randNum(min,max){//随机坐标
	return  parseInt(Math.random()*(max-min)+min)
}


function createNumball(){
	for(let i=0,len = numballArr.length;i<len;i++){
		let ball = new Numball(randNum(D,cw-D),randNum(D,ch-D),D,numballArr[i])
		ball.createBall()
		ball.moveBall()
	}
}

createNumball();



