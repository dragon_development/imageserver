/**
 * Created by Micheal on 16/10/15.
 */
$(function(){
    if($('#lt_sel_prize').html()==''){
        $('#sliderange').hide();
    }
    var maxp = '',minp='',maxpot=0,offset=0;
    $('#prizerange').live('change',function(){
        maxp = $('#maxp').html();
        minp = $('#minp').html();
        offset = $('#offset').html();
        var newoff = (offset*$(this).val()).toFixed(2)
        if( $(this).val()>0){
            var prize = (parseFloat(maxp)-parseFloat(newoff)).toFixed(2)
            var range = $(this).val();
            $('#rangevalue').html(prize+'/'+range+'%' );
            $('adjustprize').html()
            $('#lt_sel_dyprize').empty().append('<option value="'+prize+'|'+(range/100).toFixed(3)+'"  selected>'+prize+'-'+range+'%');
        }
        if($(this).val()==0){
            $('#rangevalue').html( maxp+'/'+$(this).val()+'%' );
            $('#lt_sel_findyprize').empty().append('<option value="'+maxp+'|'+$(this).val()+'"  selected>');
        }
    })
})