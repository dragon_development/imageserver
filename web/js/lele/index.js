$(document).ready(function () {

    //顶部全部游戏菜单
    if ($("#header .snav").size() > 0) {
        $("#header .snav .snav_all").hover(function () {
            $(this).children(".snav_all_menu").show();
        }, function () {
            $(this).children(".snav_all_menu").hide();
        });

        //顶部我的账户触碰菜单
        $("#myaccount").hover(function () {
            $(this).children(".myaccount_list").show();
        }, function () {
            $(this).children(".myaccount_list").hide();
        });
    }

        $("#all_game").hover(function () {
            $(".all_game_menu").show();
        }, function () {
            $(".all_game_menu").hide();
        });

        //顶部我的账户触碰菜单
        $("#myaccount").hover(function () {
            $(".myaccount_list").show();
        }, function () {
            $(".myaccount_list").hide();
        });

    //站内信效果
    if ($("#myInfo").size() > 0) {
        $("#myInfo").hover(function () {
            $(this).children(".messageShow").show();
        }, function () {
            $(this).children(".messageShow").hide();
        });

        //更新头部站内信信息
        ruiec_InsideLetter();
        setInterval(function () {
            ruiec_InsideLetter();
        }, 30000);
        ruiec_Notice();
    }
    ;
    //首页低频彩滚动
    if ($(".ibox_gp").size() > 0) {
        var oDp_li = $(".ibox_gp ul li");
        var oCont1 = oDp_li.eq(0).find(".list_cont_wrap");
        var oCont2 = oDp_li.eq(1).find(".list_cont_wrap");
        var oCont3 = oDp_li.eq(2).find(".list_cont_wrap");
        var oTimer1 = setInterval(function () {
            oCont1.stop().animate({"margin-top": "-18px"}, function () {
                $(this).append(oCont1.children("i").eq(0)).css({"margin-top": "0px"}).children("i").removeClass("curr").eq(1).addClass("curr");
            });

        }, 1500);
        var oTimer2 = setInterval(function () {
            oCont2.stop().animate({"margin-top": "-18px"}, function () {
                $(this).append(oCont2.children("i").eq(0)).css({"margin-top": "0px"}).children("i").removeClass("curr").eq(1).addClass("curr");
            });

        }, 1500);
        var oTimer3 = setInterval(function () {
            oCont3.stop().animate({"margin-top": "-18px"}, function () {
                $(this).append(oCont3.children("i").eq(0)).css({"margin-top": "0px"}).children("i").removeClass("curr").eq(1).addClass("curr");
            });

        }, 1500);

    }
    $('#menuctrl').click(function () {
        $('#topmenu').toggle();
        var src = $('#topmenu').css('display');
        if(src=='none'){
            $('#menuctrl').attr("src",'../images/comm/down.png');
            $('#allgamemenu').show();
        }else{
            $('#menuctrl').attr("src",'../images/comm/up.png');
            $('#allgamemenu').hide();
        }
    })
})
